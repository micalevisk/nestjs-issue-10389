- `npm ci`

# Test

- `npm run test:e2e`

# NodeJs Version

1. `npm run start:debug`
2. `curl -X POST "localhost:3020" --data '{"name":"bob"}' --header 'Content-Type: application/json'`

# ExpressJs Version

1. `npm run start:express`
2. `curl -X POST "localhost:3090" --data '{"name":"bob"}' --header 'Content-Type: application/json'`
