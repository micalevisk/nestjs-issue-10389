const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

app.post('/', async (req, res) => {
  const onClose = () => {
    console.log('-- Connection Closed --');
  };
  req.on('close', onClose);

  await new Promise((resolve) => setTimeout(resolve, 1000 * 1)); // 1s
  res.end('hello from express');
});

app.listen(3090, () => {
  console.log('Listening on 3090');
});
