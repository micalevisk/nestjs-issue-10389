import { NestFactory } from '@nestjs/core';
import { Module, Controller, Post, Req, Res } from '@nestjs/common';
import type { Request, Response } from 'express';
import * as bodyParser from 'body-parser';

@Controller()
export class AppController {
  static hasInvokedClosed = false; // this is for testing sake

  @Post()
  async working(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    // console.debug("listeners:", req.listeners("close")) // has the expected value
    /// This should emit, but it does not because of the body parser middleware
    const onClose = () => {
      console.log('-- Connection Closed --');
      AppController.hasInvokedClosed = true;
    };
    req.on('close', onClose);
    console.debug("listeners:", req.listeners("close")) // has the expected value

    //Terminate the connection
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1)); // 1s

    res.end('hello from nestjs');
  }
}

@Module({
  controllers: [AppController],
})
export class AppModule {}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bodyParser: !true,
  });

  /*
  app.use((req, _, next) => {
    req.on('close', () => {
      console.log('It works as a middleware!')
    })
    next()
  })
  */

  app.use(bodyParser.json());

  await app.listen(process.env.PORT || 3020);
}
if (process.env.NODE_ENV !== 'test') {
  bootstrap().catch((err) => console.error(err));
}
