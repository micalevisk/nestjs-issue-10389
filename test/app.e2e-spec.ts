import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import type { INestApplication } from '@nestjs/common';
import * as bodyParser from 'body-parser';
import { AppModule, AppController } from '../src/nestjs';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.use(bodyParser.json());

    await app.init();
  });

  afterAll(() => {
    return app.close();
  });

  beforeEach(() => {
    // refresh the state used to test the buggy behavior
    AppController.hasInvokedClosed = false;
  });

  it('POST with no body payload', async () => {
    await request(app.getHttpServer())
      .post('/')
      .expect(201)
      .expect('hello from nestjs');

    await expect(AppController.hasInvokedClosed).toBe(true);
  });

  it('POST with some body payload', async () => {
    await request(app.getHttpServer())
      .post('/')
      .send({ any: 'thing' })
      .expect(201)
      .expect('hello from nestjs');

    await expect(AppController.hasInvokedClosed).toBe(true);
  });
});
